package com.vvvtimes.util.JrebelUtil;

import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;

public class JrebelSign {
    private String signature;

    public void toLeaseCreateJson(String clientRandomness, String guid, boolean offline, String validFrom, String validUntil) {
        String serverRandomness = "H2ulzLlh7E0="; //服务端随机数,如果要自己生成，务必将其写到json的serverRandomness中
        LinkedList<String> words = new LinkedList<>();
        words.add(clientRandomness);
        words.add(serverRandomness);
        words.add(guid);
        words.add(String.valueOf(offline));

        if (offline) {
            words.add(validFrom);
            words.add(validUntil);
        }

        String s2 = StringUtils.join(words, ';');
        System.out.println(s2);
        final byte[] a2 = LicenseServer2ToJRebelPrivateKey.getPrivateKey(s2.getBytes());
        this.signature = ByteUtil.decode(a2);
    }

    public String getSignature() {
        return signature;
    }

}
