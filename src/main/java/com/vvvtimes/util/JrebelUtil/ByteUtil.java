package com.vvvtimes.util.JrebelUtil;

import org.apache.commons.codec.binary.Base64;

import java.nio.charset.StandardCharsets;
import java.util.Random;

public class ByteUtil {
    private static final Random random = new Random();

    public static String decode(final byte[] binaryData) {
        if (binaryData == null) {
            return null;
        }
        return new String(Base64.encodeBase64(binaryData), StandardCharsets.UTF_8);
    }

    public static byte[] decode(final String str) {
        if (str == null) {
            return null;
        }
        return Base64.decodeBase64(str.getBytes(StandardCharsets.UTF_8));
    }

    public static byte[] decode(final int n) {
        final byte[] array = new byte[n];
        ByteUtil.random.nextBytes(array);
        return array;
    }
}
