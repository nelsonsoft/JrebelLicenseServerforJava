package com.vvvtimes.util;


public class Base64 {
	/**
	 * 编码
	 * 
	 * @param str 字符串
	 * @return String
	 */
	public static String encode(byte[] str) {
		return org.apache.commons.codec.binary.Base64.encodeBase64String(str);
	}

	/**
	 * 解码
	 * 
	 * @param str 字符串
	 * @return string
	 */
	public static byte[] decode(String str) {
		return org.apache.commons.codec.binary.Base64.decodeBase64(str);
	}

}
