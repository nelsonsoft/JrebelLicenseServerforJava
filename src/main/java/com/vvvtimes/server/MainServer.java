package com.vvvtimes.server;

import com.vvvtimes.handler.IndexHandler;
import com.vvvtimes.handler.JetBrainsHandler;
import com.vvvtimes.handler.JrebelHandler;
import com.vvvtimes.handler.ToolHandler;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MainServer extends AbstractHandler {

    private static Map<String, String> parseArguments(String[] args) {
        if (args.length % 2 != 0) {
            throw new IllegalArgumentException("Error in argument's length ");
        }

        Map<String, String> params = new HashMap<>();

        for (int i = 0, len = args.length; i < len; ) {
            String argName = args[i++];

            if (argName.charAt(0) == '-') {
                if (argName.length() < 2) {
                    throw new IllegalArgumentException("Error at argument " + argName);
                }

                argName = argName.substring(1);
            }

            params.put(argName, args[i++]);
        }

        return params;
    }

    public static void main(String[] args) throws Exception {
        Map<String, String> arguments = parseArguments(args);
        String port = arguments.get("p");

        if (port == null || !port.matches("\\d+")) {
            port = "8081";
        }

        Server server = new Server(Integer.parseInt(port));
        server.setHandler(new MainServer());
        server.start();

        String serverStr = "http://localhost";
        if (!"80".equals(port)) {
            serverStr = serverStr + ":" + port;
        }

        System.out.println("License Server started at " + serverStr);
        System.out.println("JetBrains Activation address was: " + serverStr + "/");
        System.out.println("JRebel 7.1 and earlier version Activation address was: " + serverStr + "/{tokenname}, with any email.");
        System.out.println("JRebel 2018.1 and later version Activation address was: " + serverStr + "/{guid}(eg:http://localhost:" + port + "/" + UUID.randomUUID() + "), with any email.");

        server.join();
    }



    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            System.out.println(target);
            switch (target) {
                case "/":
                    IndexHandler.indexHandler(target, baseRequest, request, response);
                    break;
                case "/jrebel/leases":
                case "/agent/leases":
                    JrebelHandler.jrebelLeasesHandler(target, baseRequest, request, response);
                    break;
                case "/jrebel/leases/1":
                case "/agent/leases/1":
                    JrebelHandler.jrebelLeases1Handler(target, baseRequest, request, response);
                    break;
                case "/jrebel/validate-connection":
                    JrebelHandler.jrebelValidateHandler(target, baseRequest, request, response);
                    break;
                case "/rpc/ping.action":
                    JetBrainsHandler.pingHandler(target, baseRequest, request, response);
                    break;
                case "/rpc/obtainTicket.action":
                    JetBrainsHandler.obtainTicketHandler(target, baseRequest, request, response);
                    break;
                case "/rpc/releaseTicket.action":
                    JetBrainsHandler.releaseTicketHandler(target, baseRequest, request, response);
                    break;
                case "/guid":
                    ToolHandler.guidHandler(target, baseRequest, request, response);
                    break;
                default:
                    response.setStatus(javax.servlet.http.HttpServletResponse.SC_FORBIDDEN);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}